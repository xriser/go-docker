from godocker.iSchedulerPlugin import ISchedulerPlugin


class FiFo(ISchedulerPlugin):
    def get_name(self):
        return "FiFo"

    def get_type(self):
        return "Scheduler"

    def schedule(self, tasks):
        '''
        Schedule list of tasks to be ran according to user list

        :return: list of sorted tasks
        '''
        # Schedule at most max_job_pop tasks
        max_tasks = 200
        if 'max_job_pop' in self.cfg:
            max_tasks = self.cfg['max_job_pop']
        return tasks[:max_tasks]
